Run
---

```
ansible-playbook -i invetory.yml postfix_example/deploy.yml -e @secrets.yml --ask-vault-pass
```

License
-------

BSD

Author Information
------------------

Fabio Kleis
